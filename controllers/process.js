/*jslint node:true, es5:true */
'use strict';

const fork = require('child_process').fork;
const fs = require('fs');
let folders = require('../resources/folders');
let resources = require('../resources/rests');
let currentProcess = require('../resources/current-process');
let options = require('../resources/options');
let process;

exports.cleanAll = (req, res)=> {
  fs.writeFile('resources/folders.json', JSON.stringify([{"id":1,"name":"root","content":[],"folders":[]}]), (err)=>{
    if(err) res.status(500).send(err);
    fs.writeFile('resources/rests.json', JSON.stringify([]), (err)=>{
      if(err) res.status(500).send(err);
      fs.writeFile('resources/current-process.json', JSON.stringify([]), (err)=>{
        if(err) res.status(500).send(err);
        res.status(200).jsonp('CLEANED');
      });
    });
  });
};

exports.listFolders = (req, res)=> {
  let result;

  result = folders.filter((folder)=> folder.content.length);

  if(!result.length) res.status(200).send({"error":'No tiene servicios'});
  res.status(200).jsonp(result);
};

exports.listFoldersByName = (req, res)=> {
  let folderName = req.params.name;
  let result;

  result = folders.find((folder)=>folder.name === folderName);

  if(!result) res.status(500).send('Unknown folder');
  res.status(200).jsonp(result);
};

exports.listRests = (req, res)=> {
  res.status(200).jsonp(resources);
};

exports.importFolder = (req, res)=> {
  let _directory = req.params.name;
  let _indexDirectory = -1;
  let _addRests = parseInt(req.params.addRest, 10);
  let _folderImport = req.body;
  let _isExist = false;

  _indexDirectory = folders.findIndex((folder)=> folder.name === _directory);

  folders.forEach((folder)=>{
    if(folder.name === _folderImport.name) _isExist = true;
  });

  if(_isExist){
    res.status(200).send({"error":'Name of folder already exist'});
  }else if(_addRests){
    let nameOfRestDuplicate = '';
    _isExist = false;

    _folderImport.content.forEach((rest)=>{
      resources.forEach((_rest)=>{
        if(rest.name ===  _rest.name) {
          nameOfRestDuplicate =  _rest.name;
          _isExist = true
        }
      });
    });
    if(_isExist){
      res.status(200).send({"error":`Name of rest ${nameOfRestDuplicate} already exist`});
    }else {
      folders.push(_folderImport);
      folders[_indexDirectory].folders.push(_folderImport.name);

      fs.writeFile('resources/folders.json', JSON.stringify(folders), (err)=>{
        if(err) res.status(500).send(err);
        if(!_folderImport) res.status(200).jsonp(folders[_indexDirectory]);
      });

      _folderImport.content.forEach((rest)=>{
        resources.push(rest);
      });

      fs.writeFile('resources/rests.json', JSON.stringify(resources), (err)=>{
        if(err) res.status(500).send(err);
        res.status(200).jsonp(folders[_indexDirectory]);
      });
    }
  }else{
    folders.push(_folderImport);
    folders[_indexDirectory].folders.push(_folderImport.name);

    fs.writeFile('resources/folders.json', JSON.stringify(folders), (err)=>{
      if(err) res.status(500).send(err);
      res.status(200).jsonp(folders[_indexDirectory]);
    });
  }
};

exports.addGroup = (req, res)=> {
  let _isExist = false;
  let _directory = req.params.name;
  let _new = req.body;
  let _folder = {};
  let _index = -1;

  _folder = folders.find((folder)=> folder.name === _directory);
  _index = folders.findIndex((folder)=> folder.name === _directory);
  console.log(_folder);
  if(_folder.hasOwnProperty('groups')){
    _folder.groups.forEach((group)=>{
      if(group.name === _new.name) _isExist = true;
    });
  }else{
    _folder['groups'] = [];
  }

  if(_isExist){
    res.status(200).send({"error": `Group ${_new.name} already exist in the directory`});
  }else{
    _folder.groups.push(_new);
    folders[_index] = _folder;

    fs.writeFile('resources/folders.json', JSON.stringify(folders), (err)=>{
      if(err) res.status(500).send(err);
      res.status(200).jsonp(folders[_index]);
    });
  }
};

exports.addFolder = (req, res)=> {

  let _isExist = false;
  let _directory = req.params.name;
  let _new = req.body;
  let _folder = {};
  let _index = -1;

  _folder = folders.find((folder)=> folder.name === _directory);
  _index = folders.findIndex((folder)=> folder.name === _directory);

  _folder.folders.forEach((name)=>{
    if(name === _new.name) _isExist = true;
  });

  if(_isExist){
    res.status(500).send('ELEMENT DUPLICATED');
  }else{
    folders.push(_new);

    folders[_index].folders.push(_new.name);
    fs.writeFile('resources/folders.json', JSON.stringify(folders), (err)=>{
      if(err) res.status(500).send(err);
      res.status(200).jsonp(folders[_index]);
    });
  }
};

exports.addRest = (req, res)=> {

  let _isExist = false;
  let _directory = req.params.name;
  let _rest = req.body;
  let _folder = {};
  let _index = -1;

  _folder = folders.find((folder)=> folder.name === _directory);
  _index = folders.findIndex((folder)=> folder.name === _directory);

  resources.forEach((rest)=>{
    if(rest.name ===  _rest.name) _isExist = true;
  });

  if(_isExist){
    res.status(200).send({"error": `Name of rest ${_rest.name} is duplicated`});
  }else{
    resources.push(_rest);

    folders[_index].content.push(_rest);
    fs.writeFile('resources/folders.json', JSON.stringify(folders), (err)=>{
      if(err) res.status(500).send(err);
      fs.writeFile('resources/rests.json', JSON.stringify(resources), (err)=>{
        if(err) res.status(500).send(err);
        res.status(200).send(_rest);
      });
    });
  }
};

exports.deleteFolder = (req, res)=> {
  let _folderName = req.body.name;
  let _directory = req.params.name;
  let _result = [];
  let _indexElementDeleted = -1;
  let _folderIndex = -1;

  let _listIndexFoldersDelete = [];

  _indexElementDeleted = folders.findIndex((folder)=> folder.name === _folderName);
  _folderIndex = folders.findIndex((folder)=> folder.name === _directory);

  folders.forEach((folder, index)=> {
  	let indexSubFolder = folder.folders.indexOf(_folderName);
  	if(indexSubFolder !== -1) _result.push([index, indexSubFolder]);
  });

  _result.forEach((indexs)=> {
    folders[indexs[0]].folders.splice(indexs[1], 1)
  });

  getTreeIndexs(folders, _indexElementDeleted, _listIndexFoldersDelete);

  _listIndexFoldersDelete.reverse().forEach((index)=> folders.splice(index, 1));

  fs.writeFile('resources/folders.json', JSON.stringify(folders), (err)=>{
    if(err) res.status(500).send(err);
    res.status(200).jsonp(folders[_folderIndex]);
  });
};

exports.deleteRest = (req, res)=> {
  let _directory = req.params.name;
  let _rest = req.body;

  let _indexRestsFind = [];
  let _indexRest = -1;
  let _folderIndex = -1;

  _folderIndex = folders.findIndex((folder)=> folder.name === _directory);

  resources.forEach((rest, restIndex)=>{
    if(rest.name === _rest.name) _indexRest = restIndex;
  });

  folders.forEach((folder, folderIndex)=> {

  	let indexSubFolder = -1;

    folder.content.forEach((rest, restIndex)=>{
      if(rest.name === _rest.name) indexSubFolder = restIndex;
    });

  	if(indexSubFolder !== -1) _indexRestsFind.push([folderIndex, indexSubFolder]);
  });

  _indexRestsFind.forEach((index)=>{
      folders[index[0]].content.splice(index[1], 1);
  });

  resources.splice(_indexRest, 1);

  fs.writeFile('resources/folders.json', JSON.stringify(folders), (err)=>{
    if(err) res.status(500).send(err);
    fs.writeFile('resources/rests.json', JSON.stringify(resources), (err)=>{
      if(err) res.status(500).send(err);
      res.status(200).jsonp(folders[_folderIndex]);
    });
  });
};

exports.editFolder = (req, res)=> {

  let _directory = req.params.name;
  let _oldName = req.params.old;
  let _new = req.body;
  let _index = -1;
  let _indexParent = -1;
  let _indexInParent = -1;

  _index = folders.findIndex((folder)=> folder.name === _oldName);
  _indexParent = folders.findIndex((folder)=> folder.name === _directory);
  _indexInParent = folders[_indexParent].folders.findIndex((folder)=> folder === _oldName);

  folders[_index].name = _new.name;
  folders[_indexParent].folders[_indexInParent] = _new.name;

  fs.writeFile('resources/folders.json', JSON.stringify(folders), (err)=>{
    if(err) res.status(500).send(err);
    res.status(200).jsonp(folders[_indexParent]);
  });

};

exports.editRest = (req, res)=> {

  let _isExist = false;
  let _directory = req.params.name;
  let _rest = req.body;
  let _folder = {};
  let _index = -1;
  let _indexRest = -1;
  let _indexRestsFind = [];


  _folder = folders.find((folder)=> folder.name === _directory);
  _index = folders.findIndex((folder)=> folder.name === _directory);

  _folder.content.forEach((rest)=>{
    if(rest.name === _rest.name) _isExist = true;
  })

  resources.forEach((rest, restIndex)=>{
    if(rest.name === _rest.name) _indexRest = restIndex;
  });

  if(_isExist){
    resources.splice(_indexRest, 1);

    folders.forEach((folder, folderIndex)=> {
    	let indexSubFolder = -1;
      folder.content.forEach((rest, restIndex)=>{
        if(rest.name === _rest.name) indexSubFolder = restIndex;
      });

    	if(indexSubFolder !== -1) _indexRestsFind.push([folderIndex, indexSubFolder]);
    });

    _indexRestsFind.forEach((index)=>{
        folders[index[0]].content.splice(index[1], 1);
    });
  }

  resources.push(_rest);

  folders[_index].content.push(_rest);
  fs.writeFile('resources/folders.json', JSON.stringify(folders), (err)=>{
    if(err) res.status(500).send(err);
    fs.writeFile('resources/rests.json', JSON.stringify(resources), (err)=>{
      if(err) res.status(500).send(err);
      res.status(200).jsonp(folders[_index]);
    });
  });
};

function getTreeIndexs(arr, ind, list){
	list.push(ind);
	if(arr[ind].folders.length){
		arr[ind].folders.forEach((data)=>{
			let index = -1;

			arr.forEach((dataSub, ind)=>{
				if(dataSub.name === data) index = ind;
      });

			getTreeIndexs(arr, index, list);
    })
  }else{
		return;
  }
}

exports.removeGroup = (req, res)=> {
  let _directory = req.params.nameDirectory;
  let _group = req.params.nameGroup;

  let _directoryIndex = -1;
  let _groupIndex = -1;

  _directoryIndex = folders.findIndex((folder)=> folder.name === _directory);
  _groupIndex = folders[_directoryIndex].groups.findIndex((folder)=> folder.name === _group);

  folders[_directoryIndex].groups.splice(_groupIndex, 1);

  fs.writeFile('resources/folders.json', JSON.stringify(folders), (err)=>{
    if(err) res.status(500).send(err);
    res.status(200).jsonp(folders[_directoryIndex]);
  });
}

exports.removeRest = (req, res)=> {
  let _directory = req.params.name;

  let _folderIndex = -1;
  let _folder = {};

  let _restIndex = -1;
  let _rest = req.body;

  _folder = folders.find((folder)=> folder.name === _directory);
  _folderIndex = folders.findIndex((folder)=> folder.name === _directory);

  _restIndex = _folder.content.findIndex((rest)=> rest.name === _rest.name);
  folders[_folderIndex].content.splice(_restIndex, 1);
  fs.writeFile('resources/folders.json', JSON.stringify(folders), (err)=>{
    if(err) res.status(500).send(err);
    res.status(200).jsonp(folders[_folderIndex]);
  });
};

exports.addRestToFolder = (req, res)=>{
  let _directory = req.params.name;
  let _rest = req.body;

  let _folderIndex = -1;

  _folderIndex = folders.findIndex((folder)=> folder.name === _directory);

  folders[_folderIndex].content.push(_rest);
  fs.writeFile('resources/folders.json', JSON.stringify(folders), (err)=>{
    if(err) res.status(500).send(err);
    res.status(200).jsonp(folders[_folderIndex]);
  });
};

exports.listOptions = (req, res)=> {
  res.status(200).jsonp(options);
};

exports.saveOptions = (req, res)=> {
  console.log(options.port);
  console.log(req.body.port);
  options.port = req.body.port;
  fs.writeFile('resources/options.json', JSON.stringify(options), (err)=>{
    if(err) res.status(500).send(err);
    res.status(200).send('SUCCESS');
  });
};

exports.start = (req, res)=> {
  let _folderIndex = -1;
  let current = [];

  current = [...folders.find((process)=> process.name === req.params.name).content];
  _folderIndex = folders.findIndex((folder)=> folder.name === req.params.name);
  if(folders[_folderIndex].hasOwnProperty('groups')){
    folders[_folderIndex].groups.forEach((folder)=> {
      folder.content.forEach((resource)=> current.push(resource));
    });
  }

  fs.writeFile('resources/current-process.json', JSON.stringify(current), (err)=>{
    if(err) res.status(500).send(err);
    let bat = fork('index');

    bat.on('message', (message) => res.status(200).send(message));
    process=bat;
    res.status(200).send('OK')
  });
};

exports.kill = (req, res)=> {
  console.info('Finish Him!');
  if(process) process.kill();
  res.status(200).send({'msg':'Finish Him!'});
};
